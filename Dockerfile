FROM golang as builder

USER root

ENV FIGLET=figlet-2.2.5
###ENV ftp_proxy=http://proxy:3128

# install figlet
RUN wget ftp://ftp.figlet.org/pub/figlet/program/unix/${FIGLET}.tar.gz -O /tmp/${FIGLET}.tar.gz \
    && chdir /tmp \
    && tar -xzf /tmp/${FIGLET}.tar.gz \
    && chdir /tmp/${FIGLET} \
    && make install

# create archive with installed applications
RUN bash -c 'tar -czf /tmp/apps_dist.tar.gz /usr/local/bin/{chkfont,figlet,figlist,showfigfonts} /usr/local/share/{figlet,man}'

# -------
FROM debian:stretch

COPY --from=builder /tmp/apps_dist.tar.gz /tmp/
COPY files/start.sh /start.sh
###COPY files/apt.conf /etc/apt/apt.conf

RUN adduser executor --system --uid 101010 --disabled-password \
    && chdir /usr \
    && tar -xzf /tmp/apps_dist.tar.gz --strip-components 1 \
    && rm /tmp/apps_dist.tar.gz \
    && chmod +x /start.sh

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        man \
        less \
        perl \
        wget \
        curl \
        nano \
    && rm -rf /var/lib/apt/lists/*

USER executor

#CMD ["/usr/local/bin/figlet", "TADIM"]
CMD ["/start.sh"]