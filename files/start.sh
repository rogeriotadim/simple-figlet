#!/bin/bash

# if ( [ -z "$saida" ] && [ -z "$espera" ] ) || ( [ -z "$1" ] && [ -z "$2" ] )
# then
#     echo "nos parâmetros: saida e espera ou arg[1] e arg[2] devem ser definidos"
#     exit 1
# fi

echo "iniciando..."

if [ -z "$saida" ]
then
    SAIDA=$1
else
    SAIDA=$saida
fi

if [ -z "$espera" ]
then
    ESPERA=$2
else
    ESPERA=$espera
fi


if [ -z "$SAIDA" ]
then
    SAIDA="THE END"
fi

if [ -z "$ESPERA" ]
then
    ESPERA=2
fi

for i in $(eval echo "{1..$ESPERA}")
do
    ESPERA15=$(($i%15))
    ESPERA60=$(($i%60))

    sleep 1
    if [ "$ESPERA15" == "0" ]
# echo $ESPERA15
    then
        if [ "$ESPERA60" == "0" ]
        then
# echo $ESPERA60
            echo -n $(( $i / 60 ))
        else
            echo -n "."
        fi
    fi
done
echo ""

/usr/local/bin/figlet $SAIDA
